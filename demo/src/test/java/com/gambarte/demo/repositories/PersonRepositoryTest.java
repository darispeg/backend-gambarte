package com.gambarte.demo.repositories;

import com.gambarte.demo.component.TestDatabaseConfiguration;
import com.gambarte.demo.entities.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {TestDatabaseConfiguration.class})
public class PersonRepositoryTest {
    @Autowired
    private PersonRepositoryImpl personRepository;

    @Test
    void InsertPerson_shouldReturnPersonId () {
        Person person = new Person();
        person.setFirst_name("Test");
        person.setLast_name_first("Test");
        person.setLast_name_second("Test");
        person.setId_document_number("217821");
        person.setPhone("7912312");
        person.setAddress("Test");
        int result = personRepository.save(person);

        Assert.isTrue(result > 0, "No se pudo crear el usuario");
    }

    @Test
    void UpdatePerson_ShouldReturnTrue () {
        Person person = new Person();
        person.setId(4);
        person.setFirst_name("Modified");
        person.setLast_name_first("Modified");
        person.setLast_name_second("Modified");
        person.setId_document_number("887821");
        person.setPhone("7912312");
        person.setAddress("Modified");

        boolean result = personRepository.update(person);

        Assert.isTrue(result, "No se pudo actualizar el usuario");
    }

    @Test
    void FindByPersonId_ShouldReturnEntity() {
        Person person = personRepository.findById(1);

        Assert.notNull(person, "No se encontro al usuario");
    }

    @Test
    void DeleteUserById_ShouldReturnTrue() {
        boolean isDeleted = personRepository.delete(5);
        Assert.isTrue(isDeleted, "No se pudo eliminar al usuario");
    }

    @Test
    void ListPerson_ShouldReturnList() {
        List<Person> person = personRepository.findAll();

        Assert.notNull(person, "No se pudo listar a los usuarios");
    }
}
