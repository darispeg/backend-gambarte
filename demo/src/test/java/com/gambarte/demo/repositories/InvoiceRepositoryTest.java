package com.gambarte.demo.repositories;

import com.gambarte.demo.component.TestDatabaseConfiguration;
import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.Assert;

@SpringBootTest
@ContextConfiguration(classes = {TestDatabaseConfiguration.class})
public class InvoiceRepositoryTest {

    @Autowired
    private InvoiceRepositoryImpl invoiceRepository;

    @Test
    void Pagination_Test() {
        BaseFilter filter = new BaseFilter();
        filter.setPageSize(10);
        filter.setPageNumber(0);
        filter.setField("code_number");
        filter.setSearch("00");
        Pagination<Invoice> pagination = invoiceRepository.pagination(filter);

        Assert.notNull(pagination, "No funciona la paginacion");
    }
}
