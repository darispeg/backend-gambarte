package com.gambarte.demo.services;

import com.gambarte.demo.component.TestDatabaseConfiguration;
import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.service.InvoiceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = {TestDatabaseConfiguration.class})
public class InvoiceServiceTest {
    @Autowired
    private InvoiceService invoiceService;

    @Test
    void GetAllInvoices_ShouldReturnList() {
        List<Invoice> invoices = invoiceService.findAll();

        Assert.notNull(invoices, "No se encontraton comprobantes");
    }

    @Test
    void GetInvoice_ShouldReturnSuccess() {
        Invoice invoice = invoiceService.findById(1);

        Assert.notNull(invoice, "No se encontro el comprobante");
    }
}
