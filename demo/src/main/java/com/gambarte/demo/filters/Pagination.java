package com.gambarte.demo.filters;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter @Getter
public class Pagination <T> {
    public int pageSize;
    public int totalCount;
    public int currentPage;
    public int totalPage;
    public boolean hasNextPage;
    public boolean hasPreviousPage;
    public List<T> rows;
}
