package com.gambarte.demo.filters;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BaseFilter {
    public String Field;
    public String Search;
    public int PageSize;
    public int PageNumber;
}
