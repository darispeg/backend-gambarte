package com.gambarte.demo.service;

import com.gambarte.demo.entities.Person;

public interface PersonService extends GenericService<Person> { }
