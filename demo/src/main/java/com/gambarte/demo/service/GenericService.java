package com.gambarte.demo.service;

import java.util.List;

public interface GenericService <T> {
    T save(T object);
    T update(int id, T object);
    boolean delete(int id);
    T findById(int id);
    List<T> findAll();
}
