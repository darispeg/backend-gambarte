package com.gambarte.demo.service;

import com.gambarte.demo.entities.Person;
import com.gambarte.demo.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person save(Person person) {
        int newId = personRepository.save(person);

        if (newId > -1) {
            return  personRepository.findById(newId);
        }

        return null;
    }

    @Override
    public Person update(int id, Person person) {
        Person _update = personRepository.findById(id);

        _update.setFirst_name(person.getFirst_name());
        _update.setLast_name_first(person.getLast_name_first());
        _update.setLast_name_second(person.getLast_name_second());
        _update.setId_document_number(person.getId_document_number());
        _update.setPhone(person.getPhone());
        _update.setAddress(person.getAddress());

        boolean isUpdated = personRepository.update(_update);

        if (isUpdated) {
            return _update;
        }

        return null;
    }

    @Override
    public boolean delete(int id) {
        return personRepository.delete(id);
    }

    @Override
    public Person findById(int id) {
        Person person = personRepository.findById(id);
        if (person != null)
            return personRepository.findById(id);
        else
            return null;
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }
}
