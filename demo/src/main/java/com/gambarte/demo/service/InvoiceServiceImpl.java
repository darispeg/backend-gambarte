package com.gambarte.demo.service;

import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;
import com.gambarte.demo.repositories.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceRepository _invoiceRepository;

    @Override
    public Invoice save(Invoice invoice) {
        int newId = _invoiceRepository.save(invoice);

        boolean relationWithInvoice = _invoiceRepository.createRelationWithPerson(newId, invoice.getPersonId());

        if (newId > -1 && relationWithInvoice) {
            return _invoiceRepository.findById(newId);
        }

        return null;
    }

    @Override
    public Invoice update(int idInvoice, Invoice invoice) {
        Invoice _update = _invoiceRepository.findById(idInvoice);

        _update.setCode_number(invoice.getCode_number());
        _update.setDate_time(invoice.getDate_time());
        _update.setAmount(invoice.getAmount());
        _update.setExchange_rate(invoice.getExchange_rate());

        boolean isUpdate = _invoiceRepository.update(_update);

        if (isUpdate)
            return _update;

        return null;
    }

    @Override
    public boolean delete(int id) {
        return _invoiceRepository.delete(id);
    }

    @Override
    public Invoice findById(int id) {
        return _invoiceRepository.findById(id);
    }

    @Override
    public List<Invoice> findAll() {
        return _invoiceRepository.findAll();
    }

    @Override
    public Pagination<Invoice> paginationData(BaseFilter filter) {
        return _invoiceRepository.pagination(filter);
    }
}
