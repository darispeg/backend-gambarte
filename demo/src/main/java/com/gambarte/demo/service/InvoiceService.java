package com.gambarte.demo.service;

import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;

public interface InvoiceService extends GenericService <Invoice> {
    Pagination <Invoice> paginationData(BaseFilter filter);
}
