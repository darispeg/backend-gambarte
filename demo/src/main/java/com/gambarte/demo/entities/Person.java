package com.gambarte.demo.entities;

import lombok.Data;

@Data
public class Person extends BaseEntity {
    private String first_name;
    private String last_name_first;
    private String last_name_second;
    private String id_document_number;
    private String phone;
    private String address;
}
