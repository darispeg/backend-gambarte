package com.gambarte.demo.entities;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Invoice extends BaseEntity {
    private String code_number;
    private Timestamp date_time;
    private double amount;
    private String exchange_rate;
    private int personId;
}
