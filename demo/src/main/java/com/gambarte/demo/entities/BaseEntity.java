package com.gambarte.demo.entities;

import lombok.Data;

@Data
public abstract class BaseEntity {
    private int id;
}
