package com.gambarte.demo.mapper;

import com.gambarte.demo.entities.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        Person person = new Person();
        person.setId(Integer.parseInt(rs.getString("id")));
        person.setFirst_name(rs.getString("first_name"));
        person.setLast_name_first(rs.getString("last_name_first"));
        person.setLast_name_second(rs.getString("last_name_second"));
        person.setId_document_number(rs.getString("id_document_number"));
        person.setPhone(rs.getString("phone"));
        person.setAddress(rs.getString("address"));
        return person;
    }
}
