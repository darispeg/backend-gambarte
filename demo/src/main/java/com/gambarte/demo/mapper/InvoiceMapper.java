package com.gambarte.demo.mapper;

import com.gambarte.demo.entities.Invoice;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class InvoiceMapper implements RowMapper<Invoice> {

    @Override
    public Invoice mapRow(ResultSet rs, int rowNum) throws SQLException {
        Invoice invoice = new Invoice();
        invoice.setId(Integer.parseInt(rs.getString("id")));
        invoice.setCode_number(rs.getString("code_number"));
        invoice.setDate_time(Timestamp.valueOf(rs.getString("date_time")));
        invoice.setAmount(Double.parseDouble(rs.getString("amount")));
        invoice.setExchange_rate(rs.getString("exchange_rate"));
        return invoice;
    }
}
