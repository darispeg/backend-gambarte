package com.gambarte.demo.repositories;

import com.gambarte.demo.entities.Person;

public interface PersonRepository extends BaseRepository<Person> { }
