package com.gambarte.demo.repositories;

import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;

public interface InvoiceRepository extends BaseRepository <Invoice> {
    Pagination<Invoice> pagination(BaseFilter filter);

    boolean createRelationWithPerson(int invoiceId, int personId);
}
