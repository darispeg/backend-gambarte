package com.gambarte.demo.repositories;

import com.gambarte.demo.entities.Person;
import com.gambarte.demo.mapper.PersonMapper;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository{

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void postConstruct(){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(Person person) {
        try {
            String query = String.format("INSERT INTO Person " +
                    "(first_name, last_name_first, last_name_second, id_document_number, phone, address, created) " +
                    "VALUES " +
                    "('%s', '%s', '%s', '%s', '%s', '%s', CURRENT_TIMESTAMP) RETURNING id",
                    person.getFirst_name(), person.getLast_name_first(), person.getLast_name_second(),
                    person.getId_document_number(), person.getPhone(), person.getAddress());

            KeyHolder keyHolder = new GeneratedKeyHolder();

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            return connection.prepareStatement(query, new String[] {"id"});
                        }
                    }, keyHolder);

            return keyHolder.getKey().intValue();
        } catch (Exception ex) {
            System.out.printf(ex.toString());
            return -1;
        }
    }

    @Override
    public boolean update(Person person) {
        try {
            if (person.getId() > 0) {
                String sql = String.format(
                        "UPDATE person SET first_name='%s', last_name_first='%s', last_name_second='%s', id_document_number='%s', "
                                + "phone='%s', address='%s', modified=CURRENT_TIMESTAMP WHERE id='%d'",
                        person.getFirst_name(), person.getLast_name_first(), person.getLast_name_second(),
                        person.getId_document_number(), person.getPhone(), person.getAddress(), person.getId());
                jdbcTemplate.execute(sql);
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.printf(ex.toString());
            return false;
        }
    }

    @Override
    public List<Person> findAll() {
        return jdbcTemplate.query("SELECT * FROM person", new PersonMapper());
    }

    @Override
    public Person findById(int Id) {
        try {
            String query = String.format("SELECT * FROM person WHERE id='%d'", Id);
            return jdbcTemplate.queryForObject(query, new PersonMapper());
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean delete(int id) {
        try {
            String query = String.format("DELETE FROM person WHERE id='%d'", id);
            jdbcTemplate.execute(query);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
