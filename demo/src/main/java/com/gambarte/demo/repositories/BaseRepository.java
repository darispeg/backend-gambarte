package com.gambarte.demo.repositories;

import java.util.List;

public interface BaseRepository<T> {
    public int save(T object);
    public boolean update(T object);
    public List<T> findAll();
    public T findById(int Id);
    public boolean delete(int id);
}
