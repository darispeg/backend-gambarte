package com.gambarte.demo.repositories;

import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;
import com.gambarte.demo.mapper.InvoiceDetailMapper;
import com.gambarte.demo.mapper.InvoiceMapper;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class InvoiceRepositoryImpl implements InvoiceRepository {
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void postConstruct(){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(Invoice invoice) {
        try {
            String query = String.format("INSERT INTO invoice " +
                            "(code_number, date_time, amount, exchange_rate, created) " +
                            "VALUES " +
                            "('%s', '%s', '%f', '%s', CURRENT_TIMESTAMP) RETURNING id",
                    invoice.getCode_number(), invoice.getDate_time(), invoice.getAmount(), invoice.getExchange_rate());

            KeyHolder keyHolder = new GeneratedKeyHolder();

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                            return connection.prepareStatement(query, new String[] {"id"});
                        }
                    }, keyHolder);

            return keyHolder.getKey().intValue();
        } catch (Exception ex) {
            return -1;
        }
    }

    @Override
    public boolean update(Invoice invoice) {
        if (invoice.getId() > 0) {
            String sql = String.format(
                    "UPDATE invoice SET code_number='%s', date_time='%s', amount='%f', exchange_rate='%s', "
                            + "modified=CURRENT_TIMESTAMP WHERE id='%d'",
                    invoice.getCode_number(), invoice.getDate_time(), invoice.getAmount(), invoice.getExchange_rate(), invoice.getId());
            jdbcTemplate.execute(sql);

            return true;
        }

        return false;
    }

    @Override
    public List<Invoice> findAll() {
        return jdbcTemplate.query("SELECT * FROM invoice", new InvoiceMapper());
    }

    @Override
    public Invoice findById(int Id) {
        try {
            String query = String.format("SELECT i.id, i.code_number, i.date_time, i.amount, i.exchange_rate, COALESCE(pi.id_person, 0) AS id_person FROM invoice i " +
                    "LEFT JOIN person_invoice pi ON i.id = pi.id_invoice " +
                    "WHERE id='%d'", Id);
            return jdbcTemplate.queryForObject(query, new InvoiceDetailMapper());
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean delete(int id) {
        try {
            String query = String.format("DELETE FROM invoice WHERE id='%d'", id);
            jdbcTemplate.execute(query);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Pagination<Invoice> pagination(BaseFilter filter) {
        String sql = "SELECT * FROM invoice";

        if (filter.Field != null && filter.Search != null) {
            switch (filter.Field.toLowerCase()) {
                case "id" :
                    sql += String.format(" WHERE id = '%d'", Integer.parseInt(filter.Search));
                    break;
                case "code_number" :
                    sql += " WHERE code_number ILIKE '%"+ filter.Search +"%'";
                    break;
                case "first_name" :
                case "last_name_first" :
                case "last_name_second" :
                    sql += " INNER JOIN person_invoice pi ON invoice.id = pi.id_invoice " +
                            "INNER JOIN person p ON pi.id_person = p.id " +
                            "WHERE p.first_name ILIKE '%" + filter.Search +"%' OR last_name_first ILIKE '%" + filter.Search +"%' OR last_name_second  ILIKE '%" + filter.Search + "%'";
                    break;
                default: break;
            }
        }

        String queryCount = sql;
        int count = jdbcTemplate.queryForObject(queryCount.replace("*", "COUNT(*)"), Integer.class);

        sql += String.format(" LIMIT '%d' OFFSET '%d'", filter.PageSize, (filter.PageNumber * filter.PageSize));

        List<Invoice> invoices = jdbcTemplate.query(sql, new InvoiceMapper());

        Pagination<Invoice> pagination = new Pagination<>();

        pagination.setCurrentPage(filter.PageNumber);
        pagination.setPageSize(filter.PageSize);
        pagination.setTotalCount(count);
        pagination.setTotalPage((int) Math.ceil((double) count / filter.getPageSize()));
        pagination.setRows(invoices);

        pagination.setHasNextPage(invoices.size() >= (filter.getPageNumber() + 1) * pagination.pageSize);
        pagination.setHasPreviousPage(filter.getPageNumber() > 0);

        return pagination;
    }

    @Override
    public boolean createRelationWithPerson(int invoiceId, int personId) {
        try {
            if (invoiceId > 0 && personId > 0) {
                String sql = String.format(
                        "INSERT INTO person_invoice (id_person, id_invoice) VALUES ('%d', '%d')", personId, invoiceId);
                jdbcTemplate.execute(sql);

                return true;
            }

            return false;
        } catch (Exception ex) {
            return false;
        }
    }
}
