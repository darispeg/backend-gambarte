package com.gambarte.demo.controllers.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class PersonUpdateRequest {
    @NotNull @NotBlank
    private String first_name;
    @NotNull
    private String last_name_first;
    private String last_name_second;
    @NotNull @NotBlank
    private String id_document_number;
    private String phone;
    private String address;
}
