package com.gambarte.demo.controllers;

import com.gambarte.demo.controllers.models.InvoiceCreateRequest;
import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.filters.BaseFilter;
import com.gambarte.demo.filters.Pagination;
import com.gambarte.demo.service.InvoiceService;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Validated
@RestController
@RequestMapping(value = "/api/v1/invoices")
@CrossOrigin(origins = "*")
public class InvoiceController {

    @Autowired
    private InvoiceService _invoiceService;

    @GetMapping
    public ResponseEntity<Pagination<Invoice>> paginationResponse(
            @RequestParam(required = false, defaultValue = "10") int PageSize,
            @RequestParam(required = false, defaultValue = "0") int PageNumber,
            @RequestParam(required = false) String Field,
            @RequestParam(required = false) String Search) {

        BaseFilter filter = new BaseFilter();
        filter.setPageSize(PageSize);
        filter.setPageNumber(PageNumber);
        filter.setField(Field);
        filter.setSearch(Search);

        return ResponseEntity.ok(_invoiceService.paginationData(filter));
    }

    @GetMapping(value = "/{idInvoice}")
    public ResponseEntity<?> getByInvoiceId(@PathVariable int idInvoice) {
        Invoice invoice = _invoiceService.findById(idInvoice);
        if (invoice != null)
            return ResponseEntity.ok(invoice);
        else
            return ResponseEntity.badRequest().body("Comprobante no encontrado");
    }

    @PostMapping
    public ResponseEntity<?> saveInvoice(@RequestBody InvoiceCreateRequest request) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/invoices/save").toUriString());

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<InvoiceCreateRequest>> violations = validator.validate(request);

        if (!violations.isEmpty()) {
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation v : violations) {
                if (v.getPropertyPath() != null) {
                    errors.add(String.format("%s : %s", v.getPropertyPath().toString(), v.getMessage().toString()));
                }
            }
            return ResponseEntity.badRequest().body(errors.toString());
        }

        Invoice _new = new Invoice();
        _new.setCode_number(request.getCode_number());
        _new.setDate_time(request.getDate_time());
        _new.setAmount(request.getAmount());
        _new.setExchange_rate(request.getExchange_rate());
        _new.setPersonId(request.getPersonId());

        Invoice invoice = _invoiceService.save(_new);

        if (invoice != null)
            return ResponseEntity.created(uri).body(invoice);
        else
            return ResponseEntity.badRequest().body("Error al agregar el nuevo registro");
    }

    @PutMapping(value = "/{idInvoice}")
    public ResponseEntity<?> updateInvoice(@PathVariable int idInvoice, @RequestBody InvoiceCreateRequest request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<InvoiceCreateRequest>> violations = validator.validate(request);

        if (!violations.isEmpty()) {
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation v : violations) {
                if (v.getPropertyPath() != null) {
                    errors.add(String.format("%s : %s", v.getPropertyPath().toString(), v.getMessage().toString()));
                }
            }
            return ResponseEntity.badRequest().body(errors.toString());
        }

        Invoice _update = new Invoice();
        _update.setCode_number(request.getCode_number());
        _update.setDate_time(request.getDate_time());
        _update.setAmount(request.getAmount());
        _update.setExchange_rate(request.getExchange_rate());

        Invoice invoice = _invoiceService.update(idInvoice, _update);

        if (invoice != null)
            return ResponseEntity.ok().body(invoice);
        else
            return ResponseEntity.badRequest().body("Error al actualizar el nuevo registro");
    }

    @DeleteMapping(value = "/{idInvoice}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable int idInvoice) {
        boolean isDeleted = _invoiceService.delete(idInvoice);
        if (isDeleted)
            return ResponseEntity.ok("Comprobante eliminado correctamente");
        else
            return ResponseEntity.badRequest().body("Error al eliminar el registro");
    }
}
