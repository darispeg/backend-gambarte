package com.gambarte.demo.controllers;

import com.gambarte.demo.controllers.models.InvoiceCreateRequest;
import com.gambarte.demo.controllers.models.PersonCreateRequest;
import com.gambarte.demo.controllers.models.PersonUpdateRequest;
import com.gambarte.demo.entities.Invoice;
import com.gambarte.demo.entities.Person;
import com.gambarte.demo.service.PersonService;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/api/v1/person")
@CrossOrigin(origins = "*")
public class PersonController {
    @Autowired
    private PersonService _personService;

    @GetMapping
    public ResponseEntity<List<Person>> getAll() {
        return ResponseEntity.ok(_personService.findAll());
    }

    @GetMapping(value = "/{idPerson}")
    public ResponseEntity<?> getByUserKey(@PathVariable int idPerson){
        Person person = _personService.findById(idPerson);

        if (person != null)
            return ResponseEntity.ok(_personService.findById(idPerson));
        else
            return ResponseEntity.badRequest().body("Persona no encontrada");
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody PersonCreateRequest request) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/person/save").toUriString());

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PersonCreateRequest>> violations = validator.validate(request);

        if (!violations.isEmpty()) {
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation v : violations) {
                if (v.getPropertyPath() != null) {
                    errors.add(String.format("%s : %s", v.getPropertyPath().toString(), v.getMessage().toString()));
                }
            }
            return ResponseEntity.badRequest().body(errors.toString());
        }

        Person _new = new Person();
        _new.setFirst_name(request.getFirst_name());
        _new.setLast_name_first(request.getLast_name_first());
        _new.setLast_name_second(request.getLast_name_second());
        _new.setId_document_number(request.getId_document_number());
        _new.setPhone(request.getPhone());
        _new.setAddress(request.getAddress());

        Person person = _personService.save(_new);

        if (person != null)
            return ResponseEntity.created(uri).body(person);
        else
            return ResponseEntity.badRequest().body("Error al agregar el nuevo registro");
    }

    @PutMapping(value = "/{idPerson}")
    public ResponseEntity<?> updateInvoice(@PathVariable int idPerson, @RequestBody PersonUpdateRequest request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PersonUpdateRequest>> violations = validator.validate(request);

        if (!violations.isEmpty()) {
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation v : violations) {
                if (v.getPropertyPath() != null) {
                    errors.add(String.format("%s : %s", v.getPropertyPath().toString(), v.getMessage().toString()));
                }
            }
            return ResponseEntity.badRequest().body(errors.toString());
        }

        Person _update = new Person();
        _update.setFirst_name(request.getFirst_name());
        _update.setLast_name_first(request.getLast_name_first());
        _update.setLast_name_second(request.getLast_name_second());
        _update.setId_document_number(request.getId_document_number());
        _update.setPhone(request.getPhone());
        _update.setAddress(request.getAddress());

        Person person = _personService.update(idPerson, _update);

        if (person != null)
            return ResponseEntity.ok().body(person);
        else
            return ResponseEntity.badRequest().body("Error al actualizar el registro");
    }

    @DeleteMapping(value = "/{idPerson}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable int idPerson) {
        boolean isDeleted = _personService.delete(idPerson);
        if (isDeleted)
            return ResponseEntity.ok(true);
        else
            return ResponseEntity.badRequest().body(false);
    }
}
