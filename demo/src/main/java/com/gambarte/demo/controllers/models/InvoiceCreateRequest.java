package com.gambarte.demo.controllers.models;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


import java.sql.Timestamp;

@Getter @Setter
public class InvoiceCreateRequest {
    @NotNull @NotBlank
    private String code_number;

    @NotNull
    private Timestamp date_time;

    @NotNull @Min(value = 0)
    private double amount;

    @NotNull
    private String exchange_rate;

    @NotNull
    private int personId;
}
