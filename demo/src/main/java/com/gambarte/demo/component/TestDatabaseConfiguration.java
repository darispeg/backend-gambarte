package com.gambarte.demo.component;

import com.gambarte.demo.repositories.InvoiceRepositoryImpl;
import com.gambarte.demo.repositories.PersonRepositoryImpl;
import com.gambarte.demo.service.InvoiceServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class TestDatabaseConfiguration {
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/prueba");
        dataSource.setUsername("postgres");
        dataSource.setPassword("root");
        return dataSource;
    }

    @Bean
    public PersonRepositoryImpl personRepository(){
        return new PersonRepositoryImpl();
    }

    @Bean
    public InvoiceRepositoryImpl invoiceRepository(){
        return new InvoiceRepositoryImpl();
    }

    @Bean
    public InvoiceServiceImpl invoiceService() { return new InvoiceServiceImpl(); }
}
