--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-12-17 18:10:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 217 (class 1259 OID 26408)
-- Name: invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invoice (
    id integer NOT NULL,
    code_number character varying(30) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    amount numeric(10,2),
    exchange_rate character varying(30),
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone
);


ALTER TABLE public.invoice OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 26407)
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.invoice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invoice_id_seq OWNER TO postgres;

--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 216
-- Name: invoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.invoice_id_seq OWNED BY public.invoice.id;


--
-- TOC entry 215 (class 1259 OID 26354)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id integer NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name_first character varying(50) NOT NULL,
    last_name_second character varying(50),
    id_document_number character varying(30) NOT NULL,
    phone character varying(15),
    address character varying(50),
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 26353)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 214
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- TOC entry 218 (class 1259 OID 26414)
-- Name: person_invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_invoice (
    id_person integer NOT NULL,
    id_invoice integer NOT NULL
);


ALTER TABLE public.person_invoice OWNER TO postgres;

--
-- TOC entry 3183 (class 2604 OID 26411)
-- Name: invoice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice ALTER COLUMN id SET DEFAULT nextval('public.invoice_id_seq'::regclass);


--
-- TOC entry 3182 (class 2604 OID 26357)
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- TOC entry 3339 (class 0 OID 26408)
-- Dependencies: 217
-- Data for Name: invoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.invoice (id, code_number, date_time, amount, exchange_rate, created, modified) FROM stdin;
1	111110	2023-12-17 13:42:40.306	900.00	USD	2023-12-17 13:44:23.965555	\N
2	22210	2023-12-17 13:44:36.98	300.00	USD	2023-12-17 13:44:56.809743	\N
3	41225	2023-12-17 13:44:59.527	200.00	EUROS	2023-12-17 13:45:17.409769	\N
4	77877	2023-12-17 13:45:32.065	250.00	USD	2023-12-17 13:45:51.817208	\N
5	33312	2023-12-17 13:46:38.167	400.00	USD	2023-12-17 13:46:53.424555	\N
6	78887	2023-12-17 13:47:46.87	320.00	EUROS	2023-12-17 13:48:07.422646	\N
7	5552	2023-12-17 13:50:07.15	620.00	USD	2023-12-17 13:50:21.516965	\N
8	45450	2023-12-17 13:50:24.805	830.00	PESOS ARGENTINOS	2023-12-17 13:50:50.344395	\N
9	9896	2023-12-17 13:50:55.534	1000.00	USD	2023-12-17 13:51:11.961149	\N
10	63500	2023-12-17 13:51:21.039	780.00	USD	2023-12-17 13:51:43.051778	\N
11	4545	2023-12-17 13:52:22.23	1050.00	USD	2023-12-17 13:52:34.968974	\N
12	7885	2023-12-17 13:54:29.908	600.00	MXM	2023-12-17 13:54:53.192169	\N
\.


--
-- TOC entry 3337 (class 0 OID 26354)
-- Dependencies: 215
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, first_name, last_name_first, last_name_second, id_document_number, phone, address, created, modified) FROM stdin;
12	ALVARO	PACHECO	RODRIGUEZ	9786765	897667	MADRID	2023-12-17 13:35:05.611967	\N
13	DAVID	GUTIERREZ	MERIDA	56545	8756554	Av. Stright	2023-12-17 13:35:51.041236	\N
14	HUGO	CANO	FERNANDEZ	866443	5546466	Av. Camen Ortiz	2023-12-17 13:36:26.306029	\N
15	MARGALIDA	PERELLO	ROIG	798675	686856	Area 51	2023-12-17 13:36:56.063713	\N
2	DENNIS	ARISPE	GARCIA	8759253	79752455	Juliana Arias	2023-12-15 04:42:29.314549	2023-12-17 13:47:09.707954
6	HARVY	GUTIERREZ	RODRIGUEZ	875123	78902121	Av. Heroinas	2023-12-15 13:42:58.169415	2023-12-17 13:47:26.372878
11	LAURA	RAMOS	GUAMAN	1381819	3823910	Av. siempre viva	2023-12-17 02:38:22.926956	2023-12-17 13:47:41.4324
\.


--
-- TOC entry 3340 (class 0 OID 26414)
-- Dependencies: 218
-- Data for Name: person_invoice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_invoice (id_person, id_invoice) FROM stdin;
13	1
15	2
2	3
14	4
6	5
6	6
14	7
6	8
2	9
11	10
15	11
14	12
\.


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 216
-- Name: invoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.invoice_id_seq', 12, true);


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 214
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 15, true);


--
-- TOC entry 3189 (class 2606 OID 26413)
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (id);


--
-- TOC entry 3185 (class 2606 OID 26361)
-- Name: person person_id_document_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_id_document_number_key UNIQUE (id_document_number);


--
-- TOC entry 3191 (class 2606 OID 26418)
-- Name: person_invoice person_invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_invoice
    ADD CONSTRAINT person_invoice_pkey PRIMARY KEY (id_person, id_invoice);


--
-- TOC entry 3187 (class 2606 OID 26359)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- TOC entry 3192 (class 2606 OID 26424)
-- Name: person_invoice person_invoice_id_invoice_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_invoice
    ADD CONSTRAINT person_invoice_id_invoice_fkey FOREIGN KEY (id_invoice) REFERENCES public.invoice(id);


--
-- TOC entry 3193 (class 2606 OID 26419)
-- Name: person_invoice person_invoice_id_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_invoice
    ADD CONSTRAINT person_invoice_id_person_fkey FOREIGN KEY (id_person) REFERENCES public.person(id);


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE person; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,DELETE,UPDATE ON TABLE public.person TO db_user_prueba;


-- Completed on 2023-12-17 18:10:28

--
-- PostgreSQL database dump complete
--

