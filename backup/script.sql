-- crear la base de datos
CREATE DATABASE prueba

-- Crear la tabla 'persona' (person) si no existe
CREATE TABLE IF NOT EXISTS person (
	id serial PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL, -- nombres
	last_name_first VARCHAR(50) NOT NULL, -- primer apellido
	last_name_second VARCHAR(50), -- segundo apellido
	id_document_number VARCHAR(30) UNIQUE NOT NULL, -- numero de identificacion
	phone VARCHAR(15), -- celular o telefono
	address VARCHAR(50), -- direccion de domicilio
	created TIMESTAMP NOT NULL, -- fecha de creacion de los datos
	modified TIMESTAMP -- fecha de modificacion de los datos
);

-- Crear la tabla 'comprobante' (invoice) si no existe
CREATE TABLE IF NOT EXISTS invoice (
	id serial PRIMARY KEY,
	code_number VARCHAR(30) NOT NULL, -- numero de comprobante
	date_time TIMESTAMP NOT NULL, -- fecha y hora del comprobante
	amount DECIMAL(10, 2), -- monto
	exchange_rate VARCHAR(30), -- tipo de cambio
	created TIMESTAMP NOT NULL,
	modified TIMESTAMP
);

-- Crear la tabla 'persona_comprobante' (person invoice) si no existe
CREATE TABLE IF NOT EXISTS person_invoice (
	id_person INT NOT NULL,
	id_invoice INT NOT NULL,
	PRIMARY KEY (id_person, id_invoice),
	FOREIGN KEY (id_person)
		REFERENCES person (id),
	FOREIGN KEY (id_invoice)
		REFERENCES invoice (id)
);

-- crear usuario
CREATE USER db_user_prueba WITH PASSWORD 'A1b7c23#';
-- asignar privilegios al usuario
GRANT SELECT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO db_user_prueba;